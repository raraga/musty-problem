// Musty Problem # 1
// This problem was asked by Microsoft.

// Given a 2D matrix of characters and a target word, write a function that returns whether the word can be found in the matrix by going left-to-right, or up-to-down.

// For example, given the following matrix:

// [['F', 'A', 'C', 'I'],
//  ['O', 'B', 'Q', 'P'],
//  ['A', 'N', 'O', 'B'],
//  ['M', 'A', 'S', 'S']]
// and the target word 'FOAM', you should return true, since it's the leftmost column.

// Similarly, given the target word 'MASS', you should return true, since it's the last row. (edited)
//
// Notes: F = 0,0
// matrix [row][column]
// we dont need to start from 0,0? check if the first letter even exists.
// if it does return the coordinates.

const matrix = [
    ['F', 'A', 'C', 'I'],
    ['O', 'B', 'Q', 'P'],
    ['A', 'N', 'O', 'B'],
    ['M', 'A', 'S', 'S']
];

function searchMatrix(word) {

    console.log('Searching for the word: ' + word);

    const letters = word.toUpperCase().split("");
    const firstLetter = letters[0];

    // search laterally on first row...
    for (let i = 0; i < matrix.length; i++) {
        let position = i + 1;

        if (matrix[0][i] == firstLetter) {
            // using join to so we can just compare strings and not worry about the index.
            let a = matrix[i].filter((value,index) => index != 0).join('');
            let b = letters.filter((value,index) => index != 0).join('');
            a === b
                ? console.log('The word was found in row #' + position)
                : console.log('Only part of the word was found')
        } else {
            console.log('No matches found in the rows');
        }
    }


    // search vertically on first column...
    for (let i = 0; i < matrix.length; i++) {
        let position = i + 1;

        if (matrix[i][0] == firstLetter) {

            let a = matrix[i].filter((value,index) => index != 0).join('');
            let b = letters.filter((value,index) => index != 0).join('');
            a === b
                ? console.log('The first letter starts in column #' + position)
                : console.log('Only part of the word was found.')
        } else {
            console.log('No matches found in the columns');
        }
    }
}

searchMatrix('faci');
